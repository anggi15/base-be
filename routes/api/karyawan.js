const express = require('express');
const router = express.Router();

const helper = require(__class_dir + '/helper.class.js');

const fileUpload = require('express-fileupload');

const m$karyawan = require(`${__module_dir}/karyawan.module.js`);

// file upload options
router.use(fileUpload({
	useTempFiles: true,
	safeFileNames: true,
	parseNested: true,
	preserveExtension: 4,
	tempFileDir: '/tmp/'
}));

router.get('/', async function (req, res, next) {
	const list = await m$karyawan.listKaryawan();
	helper.sendResponse(res, list);
});

router.post('/', async function (req, res, next) {
	req.body.image = req.files.image
	const add = await m$karyawan.saveNewEmployee(req.body);
	helper.sendResponse(res, add);
});

router.get('/:id', async function (req, res, next) {
	const detail = await m$karyawan.getDetailKaryawan(req.params.id)
	helper.sendResponse(res, detail)
});

router.put('/:id', async function (req, res, next) {
	req.body.image = req.files.image
	
	const update = await m$karyawan.editEmployee({...req.body, id: req.params.id});
	helper.sendResponse(res, update);
});


router.delete('/:id', async function (req, res, next) {
	const deleteKaryawan = await m$karyawan.deleteKaryawan(req.params.id);
	helper.sendResponse(res, deleteKaryawan);
});

module.exports = router;

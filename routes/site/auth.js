var express = require('express');
var router = express.Router();

router.get('/login', function(req, res, next) {
  res.render('pages/login', { title: 'Express' });
});

router.post('/login');

router.get('/logout');

router.post('/logout');

module.exports = router;

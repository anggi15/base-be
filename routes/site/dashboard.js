const express = require('express');
const router = express.Router();
const date = require('date-and-time');
const path = require('path');
const hashids = new (require('hashids'))(__random);

const helper = require(__class_dir + '/helper.class.js');
const currentRouter = path.basename(__filename,  path.extname(__filename));

router.get('/', async function (req, res, next) {
	const page = {
			title : `FLOWMETER DEVICE`,
			subtitle : '---------------',
		};

	res.redirect(`${__publicurl}/ticket`);
});

module.exports = router;

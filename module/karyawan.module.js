const config = require(`${__config_dir}/app.config.json`);
const {debug} = config;
const helper = require(`${__class_dir}/helper.class.js`);
const mysql = new(require(`${__class_dir}/mariadb.class.js`))(config.db);
const __handler = require(__basedir + '/class/fileHandling.class.js');
const handler = new __handler(__basedir + '/public/image/employee/');

class _karyawan{
	async editEmployee(data){
		const self = this;
		let saveEmployee

		if (data.image) {
			const save_img = await self.upload([data.image])
	
			if (!save_img.status) {
				return save_img
			}
	
			saveEmployee = await self.updateKaryawan({
				employee_id: data.employee_id,
				name: data.name,
				date_birth: data.date_birth,
				position: data.position,
				image: save_img.data.filename,
				id: data.id
			})
		} else {
			saveEmployee = await self.updateKaryawan({
				employee_id: data.employee_id,
				name: data.name,
				date_birth: data.date_birth,
				position: data.position,
				id: data.id
			})
		}


		return {
			status: true,
			data: saveEmployee
		}
	}

	async saveNewEmployee(data){
		const self = this;

		const save_img = await self.upload([data.image])

		if (!save_img.status) {
			return save_img
		}

		const saveEmployee = await self.addKaryawan({
			employee_id: data.employee_id,
			name: data.name,
			date_birth: data.date_birth,
			position: data.position,
			image: save_img.data.filename
		})

		return {
			status: true,
			data: saveEmployee
		}
	}

	async upload(data, returnBuffer = false) {
		//no file is selected, return error
		if (data == null || data == undefined) {
            return {
                status: false,
                error: 'No files were uploaded.'
			};
		}

		//upload file
		for (let key in data) {
			try {
				let process = await handler.uploadFile(data[key]);
	
				// if (saveFile === false)
				// 	throw 'error at saving into db';
	
				// response.status = process.status;
	
				let results = {
					fileId: process.results.fileId,
					filename: process.results.filename,
					mimetype: process.results.mimetype,
					extension: process.results.extension,
					dateUploaded: process.results.dateUploaded,
					md5: process.results.md5,
					url: __siteurl.backend + '/file/getFile/' + process.results.fileId,
					path: process.results.path,
				};
	
				if (returnBuffer) {
					result.buffer = process.results.buffer;
				}
	
				return {
					status: true,
					data: results
				};
			}
			catch (error) {
				if(debug){
					console.error('upload file Error:', error);
				}
	
				return {
					status: false,
					message: error.error != undefined ? error.error:undefined,
					error
				};
			}
		}

		return response;
	}

	deleteKaryawan(id){
		const sql = {
			query: `DELETE FROM tb_employee WHERE id = ?`,
			params: [id]
		}

		return mysql.query(sql.query, sql.params)
			.then(data => {
				return {
					status: true,
					data
				}
			}).catch(error => {
				if (debug) {
					console.error('deleteKaryawan Error:', error);
				}

				return {
					status: false,
					error
				}
			})
	}

	updateKaryawan(data){
		const sql = {
			query: `UPDATE tb_employee SET name = ?, date_birth = ?, position = ?`,
			params: [data.name, data.date_birth, data.position]
		}

		if (data.image) {
			sql.query += `, image = ?`
			sql.params.push(data.image)
		}

		sql.query += ` WHERE id = ?`
		sql.params.push(data.id)

		console.log(sql);

		return mysql.query(sql.query, sql.params)
			.then(data => {
				return {
					status: true,
					data
				}
			}).catch(error => {
				if(debug){
					console.error('updateKaryawan Error:', error);
				}

				return {
					status: false,
					error
				}
			})
	}

	addKaryawan(data){
		const sql = {
			query: `INSERT INTO tb_employee(id, name, date_birth, position, image) VALUES (?, ?, ?, ?, ?)`,
			params: [data.employee_id, data.name, data.date_birth, data.position, data.image]
		}

		return mysql.query(sql.query, sql.params)
			.then(data => {
				return {
					status: true,
					data
				}
			}).catch(error => {
				if(debug){
					console.error('addKaryawan Error:', error);
				}

				return {
					status: false,
					error
				}
			})
	}

	getDetailKaryawan(id){
		const sql = {
			query: `
				SELECT
					emp.id,
					emp.name,
					emp.date_birth,
					emp.position
				FROM tb_employee emp
				WHERE emp.id = ?`,
			params: [id]
		}

		return mysql.query(sql.query, sql.params)
			.then(data => {
				return {
					status: true,
					data
				}
			}).catch(error => {
				if (debug) {
					console.error("getDetailKaryawan Error:", error);
				}

				return {
					status: false,
					error
				}
			})
	}

	listKaryawan(){
		const sql = {
			query: `
				SELECT
					emp.id,
					emp.name,
					emp.date_birth,
					emp.position
				FROM tb_employee emp
				WHERE 1`,
			params: [],
		};

		return mysql.query(sql.query, sql.params)
			.then(data => {
				return {
					status: true,
					data: data
				};
			})
			.catch(error => {
				if (error.code == "EMPTY_RESULT") {
					return {
						status: false,
						error: "Data masih kosong!"
					}
				}

				if(debug){
					console.error('karyawan list Error:', error);
				}

				return {
					status: false,
					error,
				};
			});
	};
}

module.exports = new _karyawan();

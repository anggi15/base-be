const config = require(`${__config_dir}/app.config.json`);
const {debug} = config;
const helper = require(`${__class_dir}/helper.class.js`);
const mysql = new(require(`${__class_dir}/mariadb.class.js`))(config.db);

const Xendit = require("xendit-node");
const x = new Xendit({
  secretKey: `${process.env.SECRECT_KEY}`,
});
const { Invoice } = x;
const invoiceSpecificOptions = {};
const i = new Invoice(invoiceSpecificOptions);
const callback_token = `${process.env.CALLBACK_TOKEN}`;

class _payment{
	async createInvoiceXendit(data){
        const createInvoiceXendit = await i.createInvoice({
            externalID: Date.now().toString(),
            payerEmail: data.email,
            description: data.title,
            amount: data.price,
            shouldSendEmail: true,
            currency: "IDR",
            invoiceDuration: 86400, //24 jam diubah ke menit
        });

        return createInvoiceXendit
    }

    async webhookUpdateStatus(data){
        const self = this;
        const callback_from_xendit = req.headers["x-callback-token"];

        //lakukan pengecekan token dari xendit apakah sama dengan token yang disimpan
        if (callback_token == callback_from_xendit) {
            const updateStatus = await self.updateStatusPayment({
                status: data.status,
                id: data.id
            })

            return updateStatus
        } else {
            return {
                status: false,
                error: "Not from xendit"
            }
        }
    }

    updateStatusPayment(data){
		const sql = {
			query: `UPDATE tb_employee SET status = ? WHERE id = ?`,
			params: [data.status, data.id]
		}

		return mysql.query(sql.query, sql.params)
			.then(data => {
				return {
					status: true,
					data
				}
			}).catch(error => {
				if(debug){
					console.error('updateStatusPayment Error:', error);
				}

				return {
					status: false,
					error
				}
			})
	}
}

module.exports = new _payment();
